﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ScrewfixStoreLocator.Models
{
    public interface IInboundMessageRequestDeserialiser
    {
        InboundMessageNotification Deserialise(Stream inputStream);
    }
}