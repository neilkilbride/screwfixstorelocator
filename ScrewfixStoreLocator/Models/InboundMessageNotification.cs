﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace ScrewfixStoreLocator.Models
{
    [XmlRoot("InboundMessage")]
    public class InboundMessageNotification
    {
        public Guid Id { get; set; }
        public Guid MessageId { get; set; }
        public Guid AccountId { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string MessageText { get; set; }
    }
}