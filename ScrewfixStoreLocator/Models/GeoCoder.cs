﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Xml;
using ScrewfixStoreLocator.Controllers;

namespace ScrewfixStoreLocator.Models
{
    public class GeoCoder
    {
        public static Coordinates? PostCodeToLongLat(string postcodeOrPlacename, string countryCodeWithin)
        {
            var client = new WebClient();
            var encodedPostCode = HttpUtility.UrlEncode(postcodeOrPlacename);
            var url = string.Format("https://maps.googleapis.com/maps/api/geocode/xml?address={0}&region={1}&sensor=false", encodedPostCode, countryCodeWithin);
            var xml = client.DownloadString(url);

            var doc = new XmlDocument();
            doc.LoadXml(xml);

            var nodes = doc.SelectNodes("//result");
            for (int i = 0; i < nodes.Count; i++)
            {
                XmlNode locationCheck = nodes[i].SelectSingleNode("formatted_address");

                if (!locationCheck.InnerText.EndsWith(countryCodeWithin))
                {
                    Console.WriteLine("Address not ending {0} : {1}", countryCodeWithin, locationCheck.InnerText);
                    continue;
                }


                XmlNode node = nodes[i].SelectSingleNode("geometry/location");

                double longitude;
                double lattitude;
                if (!Double.TryParse(node.SelectSingleNode("lng").InnerText, out longitude)
                    || !Double.TryParse(node.SelectSingleNode("lat").InnerText, out lattitude))
                {
                    Console.WriteLine("Problem parsing coordinates from : {0}", node.InnerText);

                    return null;
                }

                return new Coordinates
                {
                    Longitude = longitude,
                    Latitude = lattitude
                };
            }

            return null;
        }
    }
}