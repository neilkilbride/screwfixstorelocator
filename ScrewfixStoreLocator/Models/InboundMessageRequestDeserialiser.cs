﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace ScrewfixStoreLocator.Models
{
    public class InboundMessageRequestDeserialiser : IInboundMessageRequestDeserialiser
    {
        public InboundMessageNotification Deserialise(Stream inputStream)
        {
            try
            {
                var serialiser = new XmlSerializer(typeof(InboundMessageNotification), string.Empty);
                return (InboundMessageNotification)serialiser.Deserialize(inputStream);
            }
            catch (InvalidOperationException)
            {
                return null;
            }
        }
    }
}