﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ScrewfixStoreLocator.Controllers;

namespace ScrewfixStoreLocator.Models
{
    public class DistanceCalculator
    {
        public enum Units
        {
            Miles,
            Kilometres
        }

        // Uses Haversine Formula - http://en.wikipedia.org/wiki/Haversine_formula
        public static double Calculate(Coordinates from, Coordinates to, Units units)
        {
            var dLat1InRad = @from.Latitude * (Math.PI / 180.0);
            var dLong1InRad = @from.Longitude * (Math.PI / 180.0);
            var dLat2InRad = to.Latitude * (Math.PI / 180.0);
            var dLong2InRad = to.Longitude * (Math.PI / 180.0);

            var dLongitude = dLong2InRad - dLong1InRad;
            var dLatitude = dLat2InRad - dLat1InRad;

            // Intermediate result a.
            var a = Math.Pow(Math.Sin(dLatitude / 2.0), 2.0) +
                    Math.Cos(dLat1InRad) * Math.Cos(dLat2InRad) *
                    Math.Pow(Math.Sin(dLongitude / 2.0), 2.0);

            // Intermediate result c (great circle distance in Radians).
            var c = 2.0 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1.0 - a));

            // Unit of measurement
            var radius = 6371;
            if (units == Units.Miles) radius = 3959;

            return radius * c;
        }
    }
}