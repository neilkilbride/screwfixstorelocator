﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using ScrewfixStoreLocator.Models;
using com.esendex.sdk.messaging;

namespace ScrewfixStoreLocator.Controllers
{
    public struct Coordinates
    {
        public double Longitude;
        public double Latitude;
    }

    struct Store
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public Coordinates Coordinates { get; set; }
    }

    public class NotificationsController : Controller
    {
        private IInboundMessageRequestDeserialiser _inboundMessageRequestDeserialiser;
        private static List<Store> _stores = new List<Store>(); 

        public NotificationsController() : this(new InboundMessageRequestDeserialiser())
        { }

        public NotificationsController(InboundMessageRequestDeserialiser inboundMessageRequestDeserialiser)
        {
            _inboundMessageRequestDeserialiser = inboundMessageRequestDeserialiser;
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (_stores.Count == 0)
                LoadStores();

            base.OnActionExecuting(filterContext);
        }

        private void LoadStores()
        {
            string storesFile = HttpContext.Server.MapPath(@"~/App_Data/Stores.xml");

            var reader = new StreamReader(storesFile);

            var doc = new XmlDocument();
            doc.LoadXml(reader.ReadToEnd());

            var storeNodes = doc.SelectNodes("//stores/store");
            for (int i = 0; i < storeNodes.Count; i++)
            {
                var store = new Store()
                {
                    Name = storeNodes[i].SelectSingleNode("name").InnerText,
                    Address = storeNodes[i].SelectSingleNode("address").InnerText,
                    Coordinates = new Coordinates()
                    {
                        Latitude =
                            Double.Parse(
                                storeNodes[i].SelectSingleNode("location/latitude")
                                             .InnerText),
                        Longitude =
                            Double.Parse(
                                storeNodes[i].SelectSingleNode("location/longitude")
                                             .InnerText)
                    }
                };

                _stores.Add(store);
            }
        }

        public ActionResult Index()
        {
            var inboundMessage = new InboundMessageNotification()
                                     {
                                         From = "07917529157",
                                         MessageText = "Nottingham",
                                         To = "TestAccount"
                                     };

            Process(inboundMessage);

            Response.StatusCode = (int)HttpStatusCode.Accepted;
            return Content("Test message sent");
        }

        public ActionResult InboundMessage()
        {
            if (HttpContext.Request.ContentType.Contains("application/xml"))
            {
                var inboundMessage = _inboundMessageRequestDeserialiser.Deserialise(HttpContext.Request.InputStream);

                if (inboundMessage == null)
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                }
                else
                {
                    Process(inboundMessage);

                    Response.StatusCode = (int)HttpStatusCode.Accepted;
                    return new EmptyResult();
                }
            }
            else
            {
                Response.StatusCode = (int)HttpStatusCode.UnsupportedMediaType;
            }

            return Content("Unsuccessful notification");
        }

        private void Process(InboundMessageNotification message)
        {
            Double distance = Double.MaxValue;
            Store closestStore = new Store();

            var inputLocation = GeoCoder.PostCodeToLongLat(message.MessageText, "UK");

            foreach (var store in _stores)
            {
                var distanceToStore = DistanceCalculator.Calculate(inputLocation.Value, store.Coordinates, DistanceCalculator.Units.Miles);

                if (distanceToStore < distance)
                {
                    distance = distanceToStore;
                    closestStore = store;
                }
            }

            var mapImageUrl =
                string.Format(
                    "http://maps.googleapis.com/maps/api/staticmap?center={0},{1}&size=600x600&sensor=false&zoom=15&markers=color:blue%7Clabel:S%7C{0},{1}", 
                    closestStore.Coordinates.Latitude, closestStore.Coordinates.Longitude);

            var body = string.Format("Your nearest Screwfix store is:{2}{0} {1} {2} {3}", closestStore.Name, closestStore.Address, "\n", mapImageUrl);
            var recipient = message.From;

            SendMessage(recipient, body);
        }

        private static void SendMessage(string recipient, string body)
        {
            var messagingService = new MessagingService("neil.kilbride@esendex.com", "monday08");
            messagingService.SendMessage(new SmsMessage(recipient, body, "EX0009394"));
        }
    }

}
